import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-email-login',
  templateUrl: './email-login.component.html',
  styleUrls: ['./email-login.component.scss']
})
export class EmailLoginComponent implements OnInit {

  loginForm: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';
 
  constructor(public authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private snackbar: MatSnackBar) {}
 
  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required ],
      password: ['',Validators.required]
    });
  }

  tryLogin(value){
    this.authService.doLogin(value)
    .then(() => {
      this.router.navigate(['/add']);
    }, err => {
      console.log(err);
      this.snackbar.open('Your email or password that you entered is incorrect, please try again', 'Dismiss', {
        duration: 5000,
      })
      this.loginForm.reset();
    })
  }



}