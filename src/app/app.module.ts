import { NgtUniversalModule } from '@ng-toolkit/universal';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe , CommonModule} from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from '@angular/forms';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { OverlayModule } from '@angular/cdk/overlay';
import { AppRoutingModule } from './app-routing.module';

import { EditorModule } from 'primeng/editor';
import { ClipboardModule } from 'ngx-clipboard';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AddPostComponent } from './add-post/add-post.component';
import { ScrollTrackerDirective } from './directives/scroll-tracker.directive';
import { ReversePipe } from './pipes/reverse.pipe';
import { EmailComponent } from './email/email.component';
import { EmailLoginComponent } from './email-login/email-login.component';
import { AboutMeComponent } from './about-me/about-me.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddPostComponent,
    AddPostComponent,
    ScrollTrackerDirective,
    ReversePipe,
    EmailComponent,
    EmailLoginComponent,
    AboutMeComponent
  ],
  imports:[
    CommonModule,
    NgtUniversalModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatTooltipModule,
    HttpClientModule,
    FormsModule,
    MatDividerModule,
    MatListModule,
    OverlayModule,
    EditorModule,
    ClipboardModule
  ],
  providers: [DatePipe],
  entryComponents: [AboutMeComponent],
})
export class AppModule { }
