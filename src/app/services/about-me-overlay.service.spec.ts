import { TestBed, inject } from '@angular/core/testing';

import { AboutMeOverlayService } from './about-me-overlay.service';

describe('AboutMeOverlayService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AboutMeOverlayService]
    });
  });

  it('should be created', inject([AboutMeOverlayService], (service: AboutMeOverlayService) => {
    expect(service).toBeTruthy();
  }));
});
