import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Post } from '../model/post';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(public db: AngularFireDatabase) { }

  getPosts() {
    return this.db.list<Post>('posts').valueChanges();
  }

  addPosts() {
    return this.db.list<Post>('posts');
  }
}
