import { Injectable, Injector, InjectionToken } from '@angular/core';
import { Overlay, OverlayConfig } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import { AboutMeComponent } from '../about-me/about-me.component';
import { AboutMeOverlayRef } from '../about-me/about-me-overlay-ref';

export const CONTAINER_DATA = new InjectionToken<{}>('CONTAINER_DATA');

@Injectable({
  providedIn: 'root'
})
export class AboutMeOverlayService {

  constructor(private overlay: Overlay, private injector: Injector) { }

  open() {
    const overlayRef = this.createOverlay();
    const dialogRef = new AboutMeOverlayRef(overlayRef);
    const aboutMePortal = new ComponentPortal(AboutMeComponent, null, this.createInjector(dialogRef));
    overlayRef.attach(aboutMePortal);

    overlayRef.backdropClick().subscribe(_ => dialogRef.close());

    return dialogRef;
  }

  createOverlay() {
    const overlayConfig = this.getOverlayConfig();
    return this.overlay.create(overlayConfig);
  }

  getOverlayConfig(): OverlayConfig {
    const positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically();
    
    const overlayConfig = new OverlayConfig({
      hasBackdrop: true,
      backdropClass: 'cdk-overlay-dark-backdrop',
      scrollStrategy: this.overlay.scrollStrategies.block(),
      positionStrategy
    });
    return overlayConfig;
  }

  createInjector(data): PortalInjector {
    const injectorTokens = new WeakMap();
    injectorTokens.set(AboutMeOverlayRef, data);
    return new PortalInjector(this.injector, injectorTokens);
  }
}
