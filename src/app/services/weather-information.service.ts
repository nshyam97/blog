import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class WeatherInformationService {

  constructor(private http:HttpClient) { }

  getCurrentWeather() {
    const API_URL = environment.openWeather.apiURL;
    const API_KEY = environment.openWeather.apiKey;
    
    return this.http.get(API_URL + '&APPID=' + API_KEY);
  }


}
