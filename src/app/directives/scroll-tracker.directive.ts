import { WINDOW } from '@ng-toolkit/universal';
import { Directive, HostListener, HostBinding , Inject} from '@angular/core';

@Directive({
  selector: '[appScrollTracker]'
})

export class ScrollTrackerDirective {

  @HostBinding('class.shrink')
  shrinkToolbar = false;
 constructor(@Inject(WINDOW) private window: Window) {}


  @HostListener('window:scroll') onScroll() {
    if (this.window.pageYOffset >= 20) {
      this.shrinkToolbar = true;
    } else if (this.window.pageYOffset <= 0) {
      this.shrinkToolbar = false;
    }
  }

}
