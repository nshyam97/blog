import { Component } from '@angular/core';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
      this.matIconRegistry.addSvgIcon("keyboard_arrow_left", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/keyboard_arrow_left.svg"))
                          .addSvgIcon("keyboard_arrow_right", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/keyboard_arrow_right.svg"))
                          .addSvgIcon("day_atmosphere", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/ClimaconsSVG/day_atmosphere.svg"))
                          .addSvgIcon("day_clear", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/ClimaconsSVG/day_clear.svg"))
                          .addSvgIcon("day_cloudy", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/ClimaconsSVG/day_cloudy.svg"))
                          .addSvgIcon("drizzle", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/ClimaconsSVG/drizzle.svg"))
                          .addSvgIcon("heavy_thunderstorm", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/ClimaconsSVG/heavy_thunderstorm.svg"))
                          .addSvgIcon("light_snow", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/ClimaconsSVG/light_snow.svg"))
                          .addSvgIcon("night_atmosphere", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/ClimaconsSVG/night_atmosphere.svg"))
                          .addSvgIcon("night_clear", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/ClimaconsSVG/night_clear.svg"))
                          .addSvgIcon("night_cloudy", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/ClimaconsSVG/night_cloudy.svg"))
                          .addSvgIcon("rain", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/ClimaconsSVG/rain.svg"))
                          .addSvgIcon("snow", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/ClimaconsSVG/snow.svg"))
                          .addSvgIcon("github", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/logos/github-logo.svg"))
                          .addSvgIcon("gitlab", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/logos/gitlab-logo.svg"))
                          .addSvgIcon("instagram", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/logos/instagram.svg"))
                          .addSvgIcon("linkedin", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/logos/linkedin.svg"))
                          .addSvgIcon("stackoverflow", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/logos/stackoverflow.svg"))
                          .addSvgIcon("close", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/close.svg"))
                          .addSvgIcon("back", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/images/back.svg"));
  }
}
