export interface Post {
    key?: string;
    author: string;
    content: string;
    date: Object;
    title: string;

}
