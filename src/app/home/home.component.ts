import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate, query, stagger } from "@angular/animations";
import { AngularFireDatabase } from 'angularfire2/database';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { PostsService } from '../services/posts.service';
import { WeatherInformationService } from '../services/weather-information.service';
import { AboutMeOverlayService } from '../services/about-me-overlay.service';
import { Post } from '../model/post';

import { AboutMeOverlayRef } from '../about-me/about-me-overlay-ref';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    trigger('listStagger', [
      transition('* <=> *', [
        query(
          ':enter',
          [
            style({ opacity: 0, transform: 'translateY(-15px)' }),
            stagger(
              '200ms',
              animate(
                '550ms ease-out',
                style({ opacity: 1, transform: 'translateY(0px)' })
              )
            )
          ],
          { optional: true }
        ),
        query(':leave', animate('50ms', style({ opacity: 0 })), {
          optional: true
        })
      ])
    ])
  ]
})
export class HomeComponent implements OnInit {
  posts: Observable<Post[]>;
  numOfPosts: number;
  showLoading = true;
  weatherIconId: number;
  weatherIconCode: string;
  weatherIconSVGLocation: string;
  timeOfDay: string;
  svgCode: string;
  SVG_LOCATION = '../../assets/images/ClimaconsSVG/';
  

  constructor(
    private postService: PostsService,
    private weatherService: WeatherInformationService,
    public db: AngularFireDatabase,
    private overlayService: AboutMeOverlayService,
  ) {
  }

  ngOnInit() {
    this.getWeather();
    this.posts = this.postService.getPosts()
    .pipe(map(res => {
      this.numOfPosts = res.length;
      return res;
    }))
  }

  ngAfterViewInit() {
    this.posts.subscribe(() => {
      this.showLoading = false;
      setTimeout(() => {
        document.getElementById('email-component').style.visibility = 'visible';
      }, 220);
    })
  }

  displayAboutMe() {
    let dialogRef: AboutMeOverlayRef = this.overlayService.open();
  }

  getWeather() {
    this.weatherService.getCurrentWeather().subscribe((weather: any) => {
        this.weatherIconId = weather.weather[0].id;
        this.weatherIconCode = weather.weather[0].icon;
        this.timeOfDay = this.checkTimeOfDay(this.weatherIconCode);
        this.identifyCondition(this.weatherIconId);
    })
  }

  checkTimeOfDay(weatherIconCode: string) {
    return weatherIconCode.substring(2);
  }

  identifyCondition(weatherIconId: number) {
    if ( weatherIconId >= 200 && weatherIconId <= 232 ) {
      this.displayThunderstorm();
    } else if ( weatherIconId >= 300 && weatherIconId <= 531 ) {
      this.displayRainAndDrizzle(weatherIconId);
    } else if ( weatherIconId >= 600 && weatherIconId <= 622 ) {
      this.displaySnow(weatherIconId);
    } else if ( weatherIconId >= 700 && weatherIconId <= 781 ) {
      this.displayAtmosphere();
    } else {
      this.displayClearAndCloudy(weatherIconId);
    }
  }

  displayThunderstorm() {
    if ( this.timeOfDay === 'd' ) {
      this.svgCode = 'day_thunderstorm';
      this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
    } else {
      this.svgCode = 'night_thunderstorm';
      this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
    }
  }

  displayRainAndDrizzle(weatherIconId: number) {
    if ( weatherIconId <= 321 ) {
      if ( this.timeOfDay === 'd' ) {
        this.svgCode = 'day_drizzle'
        this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
      } else {
        this.svgCode = 'night_drizzle';
        this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
      }
    } else {
        if ( this.timeOfDay === 'd' ) {
          this.svgCode = 'day_rain';
          this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
        } else {
          this.svgCode = 'night_rain';
          this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
        }
    }
  }

  displaySnow(weatherIconId: number) {
    if (weatherIconId === 600 || weatherIconId === 611 || weatherIconId === 612 ) {
      if ( this.timeOfDay === 'd' ) {
        this.svgCode = 'day_light_snow';
        this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
      } else {
        this.svgCode = 'night_light_snow';
        this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg'; 
      }
    } else {
        if ( this.timeOfDay === 'd' ) {
          this.svgCode = 'day_snow'
          this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
        } else {
          this.svgCode = 'night_snow';
          this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
        }
    }
  }

  displayAtmosphere() {
    if (this.timeOfDay === 'd') {
      this.svgCode = 'day_atmosphere';
      this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
    } else {
      this.svgCode = 'night_atmosphere';
      this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
    }
  }

  displayClearAndCloudy(weatherIconId: number) {
    if (weatherIconId === 800) {
      if (this.timeOfDay === 'd') {
        this.svgCode = 'day_clear';
        this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
      } else {
        this.svgCode = 'night_clear';
        this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
      }
    } else {
      if (this.timeOfDay === 'd') {
        this.svgCode = 'day_cloudy';
        this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
      } else {
        this.svgCode = 'night_cloudy';
        this.weatherIconSVGLocation = this.SVG_LOCATION + this.svgCode + '.svg';
      }
    }
  }

}
