import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit {
  showEmailModal: boolean;

  constructor() {
  }

  ngOnInit() {
  }

  expandEmailModal() {
    this.showEmailModal = this.showEmailModal === true ? false : true;
  }
}
