import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { AngularFireAuth } from 'angularfire2/auth'
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import * as firebase from 'firebase'

import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss'],
  providers: [DatePipe]
})
export class AddPostComponent implements OnInit {

  postForm: FormGroup;

  constructor(private formBuilder: FormBuilder, 
              private postService: PostsService, 
              private datePipe: DatePipe, 
              private af: AngularFireAuth,
              private router: Router,
              private snackbar: MatSnackBar) {
                this.createForm();
              }

  ngOnInit() {
  }

  createForm() {
    this.postForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required]
    })
  }


  addPost() {
    const title = this.postForm.get('title').value;
    const content = this.postForm.get('content').value;
    const timestamp = this.getFirebaseTimestamp();
    const date = this.convertToDate(timestamp)
    const author = this.af.auth.currentUser.email;

    if (this.postForm.valid === true) {
      const itemsRef = this.postService.addPosts();
      itemsRef.push({ 
        author: author,
        content: content,
        date: date,
        title: title
      });
      this.postForm.reset();
      this.router.navigate(['/home']);
    } else {
      this.snackbar.open('Please fill in all the details', 'Dismiss', {
        duration: 2000,
      });
    }
  }

  getFirebaseTimestamp() {
    let offset;
    let offsetRef = firebase.database().ref(".info/serverTimeOffset");
    offsetRef.on("value", function(snapshot) {
      offset = snapshot.val();
    });
    return new Date().getTime() + offset;
  }

  convertToDate(timestamp: string) {
    return this.datePipe.transform(timestamp, 'dd MMM yyyy');
  }

}
