import { OverlayRef } from '@angular/cdk/overlay';

export class AboutMeOverlayRef {

constructor(private overlayRef: OverlayRef) { }

  close() {
    this.overlayRef.dispose();
  }
}
