import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { AboutMeOverlayRef } from './about-me-overlay-ref';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ClipboardService } from 'ngx-clipboard'

@Component({
  selector: 'app-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss'],
  animations: [
    trigger('slideContent', [
      state('void', style({ transform: 'translate3d(0, 25%, 0) scale(0.9)', opacity: 0 })),
      state('enter', style({ transform: 'none', opacity: 1 })),
      state('leave', style({ transform: 'translate3d(0, 25%, 0)', opacity: 0 })),
      transition('* => *', animate('400ms cubic-bezier(0.25, 0.8, 0.25, 1)')),
    ])
  ]
})
export class AboutMeComponent implements OnInit {
  animationState: 'void' | 'enter' | 'leave' = 'enter';

  constructor(private snackbar: MatSnackBar, @Inject(AboutMeOverlayRef) public componentData: any, private clipboardService: ClipboardService) { }

  ngOnInit() {
  }

  copyEmailAddress() {
    this.clipboardService.copyFromContent('nshyam668@gmail.com');

    this.snackbar.open('Copied', '', {
      duration: 1000
    });
  }

  closeOverlay() {
    this.animationState = 'leave';
    setTimeout(() => {
      this.componentData.close();
    }, 400);
  }
}
