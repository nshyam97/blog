export const environment = {
  production: true,
  firebase : {
    apiKey: "AIzaSyCJ06n3mHqIxvX9HeAcl_H_v_0Fc0yfI6s",
    authDomain: "blog-4ac72.firebaseapp.com",
    databaseURL: "https://blog-4ac72.firebaseio.com",
    projectId: "blog-4ac72",
    storageBucket: "",
    messagingSenderId: "568232530532"
  },
  openWeather : {
    apiKey: "cba1a2a99dc6a4e4d34f219aa6907f1e",
    apiURL: "http://api.openweathermap.org/data/2.5/weather?id=2648208&units=metric",
  }
};
