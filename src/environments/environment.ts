// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // Initialize Firebase
  firebase : {
    apiKey: "AIzaSyCJ06n3mHqIxvX9HeAcl_H_v_0Fc0yfI6s",
    authDomain: "blog-4ac72.firebaseapp.com",
    databaseURL: "https://blog-4ac72.firebaseio.com",
    projectId: "blog-4ac72",
    storageBucket: "",
    messagingSenderId: "568232530532"
  },
  openWeather : {
    apiKey: "cba1a2a99dc6a4e4d34f219aa6907f1e",
    apiURL: "http://api.openweathermap.org/data/2.5/weather?id=2648208&units=metric",
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
